var app = angular
  .module
  ('app', [
    'lbServices',
    'ui.router', 'ngAnimate'
  ]);
  app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'views/home.html'
      })
      .state('destination', {
        url: '/destinations/:id',
        templateUrl: 'views/destinations/destination.html',
        controller: 'DestinationController'
      })
      .state('question', {
        url: '/questions/:id',
        templateUrl: 'views/questions/questions.html',
        controller: 'QuestionController'
      })
      .state('results', {
        url: '/results',
        templateUrl: 'views/results.html',
        controller: 'ResultsController'
      })
      .state('answers', {
        url: '/answers',
        templateUrl: 'views/answers/answers.html',
        controller: 'AnswerController'
      });

    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
  }]);

  

