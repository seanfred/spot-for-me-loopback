angular
  .module('app')
  .controller('DestinationController', ['$rootScope','$scope', '$state', 'Destination', '$http', 'selected', 'selectedNames', '$stateParams', '$location', function($rootScope, $scope, $state, Destination, $http, selected, selectedNames, $stateParams, $location) {
    $scope.id = $stateParams.id;
    $scope.destinations = Destination.find();

    Destination
    .findById({
      id: $scope.id
    }, function(data){
        $scope.destination = data;
    })

    $scope.singleDest = function(){
      var singleDest = this.destination.id;
      $location.path('/destinations/' + singleDest);
    };
    
    

  }]);

