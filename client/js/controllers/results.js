angular
  .module('app')
  .controller('ResultsController', ['$rootScope','$scope', '$state', 'Destination', '$http', 'selected', 'selectedNames', '$location', function($rootScope, $scope, $state, Destination, $http, selected, selectedNames, $location) {
    $rootScope.destinations = [];

    function getDestinations() {
      Destination
        .find()
        .$promise
        .then(function (results) {
          $rootScope.destinations = results;
        });
    }
    getDestinations();

    $scope.next = function(){
      $location.path('/destinations/1');
    };

    $scope.captureEmail = function(){
      var email = false;

      return email;
    };  


  }]);

