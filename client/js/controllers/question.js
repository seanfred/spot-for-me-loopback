angular
  .module('app')
  .controller('QuestionController', ['$rootScope','$scope', '$state', 'Question', '$http', '$stateParams', '$location', 'selected', 'selectedNames', function($rootScope,$scope, $state, Question, $http, $stateParams, $location, selected, selectedNames) {
    $scope.id = $stateParams.id;

    Question
    .findById({
    	id: $scope.id
    }, function(data){
        $scope.question = data;
    })

    Question.answers
    ({
     	id: $scope.id
    }, function(data){
        $scope.answers = data;
    })

    Question.count(function(data){
        $scope.count = data.count;
    })

    $scope.removePick = function(){
        var id = parseInt($scope.id);
        var previous = id - 1;
        var currentPath = $location.path();
        if(currentPath !== '/questions/1'){
            selected.pop();
            selectedNames.pop();
            $location.path('/questions/' + previous);
            console.log(modeString(selected));
        }
    };

    $scope.firstQuestion = function(){
        var question = true;
        var path = $location.path();
        if(path === '/questions/1'){
            question = false;
        }

        return question;
    };

    $scope.addPick = function(){
        var id = parseInt($scope.id);
        var pick = parseInt(this.pick);
        var pickName = this.answer.answer;
    	var next = id + 1;
    	var currentPath = $location.path();
    	if(currentPath === '/questions/' + $scope.count){
            selected.push(pick);
            selectedNames.push(pickName);
            console.log(selectedNames);
            console.log(modeString(selected));
    		$rootScope.group = selected;
            $rootScope.selectedNames = selectedNames;
    		$location.path('/results');
    	}
    	else{
        selected.push(pick);
        selectedNames.push(pickName);
        console.log(selectedNames);
        console.log(modeString(selected));
        $location.path('/questions/' + next);
    	}	

    	};

    $scope.test = function(){
        console.log(this.pick);
    };

    function modeString(array){	
    if (array.length == 0)
        return null;

    var modeMap = {},
        maxEl = array[0],
        maxCount = 1;

    for(var i = 0; i < array.length; i++)
    {
        var el = array[i];

        if (modeMap[el] == null)
            modeMap[el] = 1;
        else
            modeMap[el]++;

        if (modeMap[el] > maxCount)
        {
            maxEl = el;
            maxCount = modeMap[el];
        }
        else if (modeMap[el] == maxCount)
        {
            maxEl += '&' + el;
            maxCount = modeMap[el];
        }
    }
    return maxEl;
	}
    
  }]);

